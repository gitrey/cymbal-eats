package org.google.demo;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
public class MenuTest {

    @Test
    void testFindReady(){
        List<Menu> readyMenus = Menu.findReady();
        assertTrue(readyMenus.size()>=0);
    }
}